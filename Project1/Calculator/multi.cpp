#include <iostream>
#include "Calculator.h"
using namespace std;

/*
두 수를 입력받아 곱셈으로 출력
0 0을 입력받으면 종료
몇 번 작동했는지 출력
*/

int main()
{
	int a, b, cnt = 0;
	Calculator cal;
	while (true)
	{
		cout << "두 개의 숫자 입력: ";
		cin >> a >> b;	// 두 수 입력

		if (a != 0 || b != 0) // 0 0인가 판단
		{
			cout << "연산 결과: " << cal.multiplication(a, b) << endl; // 출력
			cnt++;	// 횟수 증가
		}
		else
			break; // 0 0이면 반복문 탈출
	}
	cout << cnt << "번 연산했습니다." << endl; // 횟수 출력
	system("pause");
}