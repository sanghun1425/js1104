#include <iostream>
#include "monster.h"
#include "Paper.h"
#include "Scissors.h"
#include "Rock.h"
using namespace std;

int main()
{
	monster mon;
	Paper paper;
	Rock rock;
	Scissors scissors;

	int battle = mon.Who();
	int choice;
	while (true)
	{
		switch (battle)
		{
		case 1:
			cout << "가위 킹이 등장했습니다." << endl;
			break;
		case 2:
			cout << "바위 킹이 등장했습니다." << endl;
			break;
		case 3:
			cout << "보 킹이 등장했습니다." << endl;
		}
		cout << "1. 가위 2. 바위 3. 보 중에서 선택해 주세요(10를 입력하면 게임종료): ";
		cin >> choice;
		if (choice == 10)
		{
			cout << "게임을 종료합니다.";
			return 0;
		}
		switch (battle)
		{
		case 1:
			scissors.Battle();
			scissors.Result(choice);
			scissors.ShowHP();
			if (scissors._HP <= 0)
				return 0;
			break;
		case 2:
			rock.Battle();
			rock.Result(choice);
			rock.ShowHP();
			if (rock._HP <= 0)
				return 0;
			break;
		case 3:
			paper.Battle();
			paper.Result(choice);
			paper.ShowHP();
			if (paper._HP <= 0)
				return 0;
			break;
		}
		system("pause");
		system("cls");
	}
}