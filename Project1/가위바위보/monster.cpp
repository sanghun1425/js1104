#include "monster.h"
#include <time.h>
#include <iostream>
using namespace std;



monster::monster()
{
	_choice = 0;
	_HP = 200;
}


monster::~monster()
{
}

void monster::Result(int param)
{
	cout << "사용자: ";
	if (param == 1) cout << "가위";
	else if (param == 2) cout << "바위";
	else cout << "보";
	cout << " vs 봇: ";
	if (_choice == 1) cout << "가위" << endl;
	else if (_choice == 2) cout << "바위" << endl;
	else cout << "보" << endl;

	
	if (param == _choice)
		cout << "비겼습니다." << endl;
	if (param + 1 == _choice || param == 3 && _choice == 1)
	{
		cout << "졌습니다." << endl;
		cout << "데미지를 10만큼 입었습니다." << endl;
	}
	if (param - 1 == _choice || param == 1 && _choice == 3)
	{
		cout << "이겼습니다." << endl;
		_damage = rand() % 100 + 1;
		cout << "데미지를 " << _damage << "만큼 입혔습니다." << endl;
		_HP -= _damage;
		if (_HP < 0)
			_HP = 0;
	}
}

void monster::ShowHP()
{
	cout << "보스의 현재 남은 체력: " << _HP << endl;
}

int monster::Who()
{
	srand(time(NULL));
	_random = (rand() % 10) % 3 + 1;
	return _random;
}