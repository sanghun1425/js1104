#include <iostream>
using namespace std;

#define INPUT_SIZE 10

void InputData(int*, int*, int*, int*, int*);
void OutputData(int*, int*);

int main()
{
	int temp[INPUT_SIZE] = { 0 };

	int even[5] = { 0 };
	int odd[5] = { 0 };

	int evenindex = 0;
	int oddindex = 0;

	InputData(temp, even, odd, &evenindex, &oddindex);
	OutputData(even, odd);
}

void InputData(int* temp, int* even, int* odd, int* evenindex, int* oddindex)
{
	for (int i = 0; i < 10; i++)
	{
		cin >> temp[i];

		if (temp[i] % 2 == 0)
		{
			even[(*evenindex)] = temp[i];
			(*evenindex)++;
		}
		else
		{
			odd[(*oddindex)] = temp[i];
			(*oddindex)++;
		}
	}
}

void OutputData(int* even, int* odd)
{
	cout << "¦��: ";
	for (int i = 0; i < 5; i++)
	{
		cout << even[i] << " ";
	}
	cout << endl << "Ȧ��: ";
	for (int i = 0; i < 5; i++)
	{
		cout << odd[i] << " ";
	}
	cout << endl;
	system("pause");
}