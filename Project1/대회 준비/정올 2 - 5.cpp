﻿#include <iostream>
using namespace std;

/*
지뢰찾기

입력예시
3
..*
*..
.*.

출력예시
12*
*32
2*1 
*/

int main()
{
	int n, cnt, result[9][9] = { 0 };
	char mine[9][9] = { 0 };
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		for(int v = 0; v < n; v++)
		{
			cin >> mine[i][v];
		}
	}
	
	for(int i = 0; i < n; i++)
	{
		for(int v = 0; v < n; v++)
		{
			if(mine[i][v] != '*')
			{
				cnt = 0;
				for(int row = -1; row <= 1; row++)
				{
					for(int col = -1; col <= 1; col++)
					{
						if(mine[i + row][v + col] == '*')
							cnt++;
					}
				}
				result[i][v] = cnt;
			}
			else
				result[i][v] = mine[i][v];
		}
	}
	
	for(int i = 0; i < n; i++)
	{
		for(int v = 0; v < n; v++)
		{
			if(result[i][v] == 42)
				cout << '*';
			else
				cout << result[i][v];
		}
		cout << endl;
	}
}
