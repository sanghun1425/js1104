﻿#include <iostream>
using namespace std;
/*
물건을 사고 난 뒤 거스름돈으로 줄 동전의 개수를 최소로 한다.
마지막 500원 120원 100원 50원 1원 개수 출력
입력예시
1000 640
출력예시
500 0
120 3
100 0
50 0
1 0
총 3개 
*/
int main()
{
	int pay, price, money, sum = 0;
	int coin[5]={500,120,100,50,10};//각각 동전의 금액
	int cnt[5]={0,0,0,0,0};//각각 동전마다 갯수
	cin >> pay >> price;
	money = pay - price;
	for(int i = 0; i < 5; i++)
	{
		while(money >= coin[i])
		{
			money -= coin[i];
			cnt[i]++;
		}
	}
	for(int i = 0; i < 5; i++)
	{
		cout << coin[i] << ' ' << cnt[i] << endl;
		sum = sum + cnt[i];
	}
	cout << "총 " << sum << "개";
}
