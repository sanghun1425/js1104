﻿#include <iostream>
using namespace std;

/*
상금판을 밟으며 도착점까지 가는데 최대 상금을 구하라
단 연속적으로 3개의 발판을 밟을 수 없고 이동은 한칸 또는 두칸이동한다. 
입력예시 
6
10
20
15
25
10
20

8
10
35
15
25
30
20
10
5

110
출력예시
75 
*/

int main()
{
	int n, pan[300] = {0}, sum = 0, cnt = 0;
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		cin >> pan[i];
	}
	
	for(int i = 0;pan[i] != 0;)
	{
		if(pan[i] > pan[i + 1] && cnt != 2 || pan[0] + pan[1] < pan[2] + pan[3] && i == 0)
		{
			sum += pan[i];
			cnt++;
			i++;
		}
		else
		{
			sum += pan[i + 1];
			cnt = 1;
			i += 2;
		}
	}
	system("cls");
	cout << sum;
}
