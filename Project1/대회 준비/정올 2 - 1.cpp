﻿#include <iostream>
using namespace std;

/*
스티커가 12개 이상이고 그중 담임선생님 스티커가 4개 이상이면
스티커 12개당 선물하나를 받는다.

입력예시:
3
10 28
4 11
12 12

출력예시:
2
0
1 
*/

int main()
{
	int n, dstiker, allstiker, result[100] = { 0 };
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		cin >> dstiker >> allstiker;
		if(dstiker >= 4 && allstiker >= 12)
			result[i] = allstiker / 12;
	}
	
	for(int i = 0; i < n; i++)
	{
		cout << result[i] << endl;
	}
}
