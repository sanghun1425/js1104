﻿#include <iostream>
using namespace std;

/*
팀당 1명씩 점수가 높은 순서대로 금메달, 은메달, 동메달을 준다.

입력예시
9
1 1 95
1 2 65
1 3 71
2 1 86
2 2 82
2 3 80
3 1 90
3 2 88
3 3 38

출력예시
금메달 1 1 95
은메달 3 1 90
동메달 2 1 86 
*/

int main()
{
	int n, student[11][11] = { 0 }, score[100], num[100], team[100];
	int firstteam, firstnum, secondteam, secondnum, thirdteam, thirdnum;
	int max = 0, second = 0, third = 0;
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		cin >> team[i] >> num[i] >> score[i];
		student[team[i]][num[i]] = score[i];
	}
	
	for(int i = 1; i <= 10; i++)
	{
		for(int v = 1; v <= 10; v++)
		{
			if(student[i][v] != 0)
			{
				if(student[i][v] > max)
				{
					max = student[i][v];
					thirdteam = secondteam;
					thirdnum = secondnum;
					secondteam = firstteam;
					secondnum = firstnum;
					firstteam = i;
					firstnum = v;
				}
				else if(student[i][v] > second && firstteam != i)
				{
					second = student[i][v];
					thirdteam = secondteam;
					thirdnum = secondnum;
					secondteam = i;
					secondnum = v;
				}
				else if(student[i][v] > third && firstteam != i && secondteam != i)
				{
					third = student[i][v];
					thirdteam = i;
					thirdnum = v;
				}
			}
		}
	}
	cout << "금메달 " << firstteam << ' ' << firstnum << ' ' << student[firstteam][firstnum] << endl;
	cout << "은메달 " << secondteam << ' ' << secondnum << ' ' << student[secondteam][secondnum] << endl;
	cout << "동메달 " << thirdteam << ' ' << thirdnum << ' ' << student[thirdteam][thirdnum];
}
