﻿#include <iostream>
using namespace std;

/*
자연수의 k번째 약수를 구하라

입력예시:
3
6 3
9 4
11 2

출력예시:
3
0
11 
*/

int main()
{
	int n, a, b, cnt, result[100] = { 0 };
	cin >> n;
	for(int i = 0 ;i < n; i++)
	{
		cin >> a >> b;
		cnt = 0;
		for(int v = 1; v <= a; v++)
		{
			if(a % v == 0)
			{
				cnt++;
				if(cnt == b)
					result[i] = v;
			}
		}
	}
	
	for(int i = 0; i < n; i++)
	{
		cout << result[i] << endl;
	}
}
