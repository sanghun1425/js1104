#include <iostream>
using namespace std;
/*
2진수로 변환하여 1의 개수가 같은 가장 가까운 큰수 작은 수 구하기 

입력예시
10

출력예시
9 12
*/
int main()
{
	int a, small, big, cnt = 0, acnt = 0, b;
	cin >> a;
	b = a;
	for(int i = a; i > 0; i--)
	{
		if(a % 2 == 1)
			acnt++;
		a = a / 2; 
	}
	for(int i = b - 1; i > 0; i--)
	{
		cnt = 0;
		small = i;
		while(small != 0)
		{
			if(small % 2 == 1)
				cnt++;
			small = small / 2;
		}
		if(acnt == cnt)
		{
			small = i;
			break;
		}
	}
	for(int i = b + 1;; i++)
	{
		cnt = 0;
		big = i;
		while(big != 0)
		{
			if(big % 2 == 1)
				cnt++;
			big = big / 2;
		}
		if(acnt == cnt)
		{
			big = i;
			break;
		}
	}
	cout << small << ' ' << big;
}
